%   function  demo.m %------------------------------------------------
% функция  демонстрирующая работы программы - 
% вычисление интеграла 
% по заданной таблично функции
function demo()
% заданная таблично функция
x = [0 0.50 1 1.5 2 2.5 3 3.5 4 4.5];
y = [1.5 1.70 1.1 0.67 0.6 0.9 1 0.78 0.24 -0.2];
% пределы интегрирования 
xmin = 0.5; xmax = 3.5;

% выбор табличных данных с учетом пределов интегрирования
% В X и Y массивах значения 
% усекаются до пределов интегрирования.
[r,c] = find(x >= xmin & x <= xmax);
X = x(c);
Y = y(c);

% вывод в консоль аргумента,  заданной таблично функции, 
% пределов  и шага интегрирования
fprintf('%s\n',' ');
disp('Таблично заданная функция');
disp('аргумент x:');
fprintf('(%s)\n', strjoin(cellstr(num2str(x(:))),', '));
disp('значения функции y(x):');
fprintf('(%s)\n', strjoin(cellstr(num2str(y(:))),', '));
h = x(2:1:end) - x(1:1:end -1);
disp('шаг интегрирования:');
fprintf('(%s)\n', strjoin(cellstr(num2str(h(:))),', '));
fprintf('нижний предел интегрирования %2.1f\n',xmin);
fprintf('верхний предел интегрирования %2.1f\n',xmax);
% вычисление  интеграла методом трапеций
Q = trapz(X, Y);
fprintf('Значение определенного интеграла: %.4f\n',Q);

% вызов функции для вывода графика подынтегральной функции и пределов 
%интегрирования 
h = plotint(x,y, xmin, xmax);
% задание заголовка на графике 
s = strcat('Значение интеграла = ',num2str(Q));
title(h,s); 
end
%   end function  demo.m %---------------------------------------------
