% function submenu(h) ------------------------------------------------
% Функция дополнительного меню для выбора 
% опций меню и редактирования линии
function submenu(h) 
% в цикле - вывод меню и выбор пользователем соответствующей опции
 
while (true)
    disp('Mеню редактирования толщины и цвета линии');
    disp('1 цвет линии');
    disp('2 толшина линии');
    disp('3 выход из редактирования');
    try
         % ввод пользователя
          keystr = input('Выберите один из пунктов меню (цифры 1-3):','s');
         % форматирование в число
          key =  str2double(keystr);
          assert((key < 4) && (key > 0))% проверка на ошибки
      
    catch % если ошибка
        % вывод в консоль сообщения об ошибке 
        display('Ошибка ввода, допустимые цифры 1-3');
        input('Для продолжения нажмите Enter','s');
        
    end
% списки цвета и толшщины линии графика      
 listcolor = {'red','yellow','blue','white',...                   
'green','black','magenta','cyan'};
 listwidth  = {'0.5','1','1.5','2',...                   
'2.5','3','3.5','4','4.5','5'};
% обработка ошибки, если графическое окно закрыто
try
   m = xlim(h); 
catch
    display('Графическое окно закрыто');
    input('Для продолжения нажмите Enter','s');
    return;
end




    switch key % выбор введенного пункта меню 
        % и выбор из списка 
         case 1  
              [indx,tf] = listdlg('Name','Color','SelectionMode','single',...
                  'ListSize',[150,150],'ListString',listcolor);
               if (tf == 0) 
                   break; end
               set(h.Children(3),'Color',listcolor{indx});view(2);
               
         case 2
              [indx,tf] = listdlg('Name','Size','SelectionMode','single',...
                  'ListSize',[150,150],'ListString',listwidth);
               if (tf == 0) 
                   break; end
               set(h.Children(3),'LineWidth',str2double(listwidth{indx}));
               
          case 3 % выход
              break; 
      otherwise 
            continue;
    end
       
end
    

 
 
end % function submenu(h) -------------------------------------------- 