% function  plotint.m %----------------------------------------------
% функция  для вывода графика под интегральной функции
% и границ интегрирования
% входные аргументы
% x,y - таблично заданная  функция
% xmin, xmax - интервалы интегрирования
% функция возвращает дескриптор текущего окна
function h = plotint(x, y, xmin, xmax)

figure, % вывод окна графика
plot(x,y,'r','LineWidth',2.5);
grid on;
% вывод пределов интегрирования в виде вертикальных линий 
hold on;
line([xmin xmin], ylim,'Color','green','LineStyle','--','LineWidth',1.0);
line([xmax xmax], ylim,'Color','green','LineStyle','--','LineWidth',1.0);
hold off;
% ограничение области графика по координате х
xlim([min(x) max(x)]); 
xlabel('x'); ylabel('y');
h = gca; % возвращаем дескриптор текущего окна
end
% end function  plotint.m %--------------------------------------------